import "./App.css";

const rowNumber = 76;
const colNumber = 90;
const colors = ["#2f9168", "#9eedd7", "#46bcb0", "#54baa2", "#2cdcd2"];
const getRandomColor = () => colors[Math.floor(Math.random() * colors.length)];
const times = <T extends unknown>(times: number, hydrator: () => T) =>
  Array(times).fill(null).map(hydrator);
const venecitas: string[][] = times(rowNumber, () =>
  times(colNumber, getRandomColor)
);

const App = () => {
  return (
    <div class="App">
      <div class="wall">
        <div class="venecitas">
          {venecitas.map((row) => (
            <div class="row">
              {row.map((c) => (
                <div class="venecita" style={`background-color: ${c};`} />
              ))}
            </div>
          ))}
        </div>
        <div class="railing-wrapper">
          <div class="railing" />
        </div>
      </div>
      <div class="stairs">
        <div class="floor">
          <div class="row">
            <div class="tile" />
            <div class="tile" />
            <div class="tile" />
            <div class="tile" />
          </div>
          <div class="row">
            <div class="tile" />
            <div class="tile" />
            <div class="tile" />
            <div class="tile" />
          </div>
        </div>
        <div class="second step"></div>
        <div class="first step"></div>
        <div class="top step"></div>
      </div>
    </div>
  );
};

export default App;
